package com.warthur.vertx.demo.pojo;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

/**
 * @author warthur
 * @date 2019/10/11
 */
public class AuthUser implements User {

    private JsonObject authInfo;

    public AuthUser(JsonObject authInfo) {
        this.authInfo = authInfo;
    }

    @Override
    public User isAuthorized(String s, Handler<AsyncResult<Boolean>> handler) {
        handler.handle(Future.succeededFuture(true));
        return this;
    }

    @Override
    public User clearCache() {
        return null;
    }

    @Override
    public JsonObject principal() {
        return authInfo;
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {

    }
}
