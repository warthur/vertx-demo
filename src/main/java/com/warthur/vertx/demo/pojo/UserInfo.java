package com.warthur.vertx.demo.pojo;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.Data;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author warthur
 * @date 2019/10/12
 */
@Data
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -1432951946285309582L;

    private static final AtomicInteger ACC = new AtomicInteger(0);

    private String userId;
    private String userName;
    private String openId;
}
