package com.warthur.vertx.demo.excepton;

import com.warthur.vertx.demo.common.enums.ErrorCode;

/**
 * @author warthur
 * @date 2019/10/10
 */
public class SystemException extends RuntimeException {
    private static final long serialVersionUID = -46383982132485168L;

    private int code;

    private SystemException(int code, String message) {
        super(message);
        this.code = code;
    }

    public SystemException(String message) {
        this(ErrorCode.REQUEST_ERROR.getCode(), message);
    }

    public SystemException(ErrorCode errorCode) {
        super(errorCode.getMsg());
        this.code = errorCode.getCode();
    }
}
