package com.warthur.vertx.demo.service;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceProxyBuilder;

/**
 * @author warthur
 * @date 2019/10/12
 */
public interface SomeService {

    String SERVICE_ADDRESS = "service.example";

    static SomeService createService(Vertx vertx, JsonObject config) {
        return null;
    }

    static SomeService createProxy(Vertx vertx) {
        return new ServiceProxyBuilder(vertx).setAddress(SERVICE_ADDRESS).build(SomeService.class);
    }

    @Fluent
    SomeService process(String id, Handler<AsyncResult<JsonObject>> resultHandler);
}
