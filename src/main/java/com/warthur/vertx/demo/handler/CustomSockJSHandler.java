package com.warthur.vertx.demo.handler;

import io.vertx.ext.web.handler.sockjs.SockJSSocket;

/**
 * @author warthur
 * @date 2019/10/11
 */
public class CustomSockJSHandler {

    public static void handle(SockJSSocket context) {
        context.handler(context::write);
    }
}
