package com.warthur.vertx.demo.common.utils;

import com.warthur.vertx.demo.common.Response;
import com.warthur.vertx.demo.common.enums.ErrorCode;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author warthur
 * @date 2019/10/07
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResponseUtils {

    public static Response build(RoutingContext context) {

        return new Response(context);
    }
}
