package com.warthur.vertx.demo.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统响应状态码
 * @author admin
 * @date 2017/7/7
 */
@Getter
@AllArgsConstructor
public enum ErrorCode {

    /**
     * 错误状态码-错误信息
     */
    REQUEST_SUCCESS(0, "请求成功"),
    REQUEST_ERROR(10001, "请求失败，数据处理异常"),
    REQUEST_PARAM_ERROR(10002, "请求数据校验失败"),
    REQUEST_METHOD_ERROR(10003, "请求方式非法"),
    AUTH_TOKEN_ERROR(10004, "请求token非法");

    private Integer code;
    private String msg;
}
