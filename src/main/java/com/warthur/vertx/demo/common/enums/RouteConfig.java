package com.warthur.vertx.demo.common.enums;

import com.warthur.vertx.demo.handler.HelloHandler;
import com.warthur.vertx.demo.handler.SecurityHandler;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author warthur
 * @date 2019/04/09
 */
@Getter
@AllArgsConstructor
public enum RouteConfig {

    HELLO(HttpMethod.GET, "/hello", HelloHandler::hello, false),
    USER_LIST(HttpMethod.GET, "/users", HelloHandler::handle, false),
    BODY(HttpMethod.POST, "/body", HelloHandler::body, true),
    TOKEN(HttpMethod.GET, "/token", SecurityHandler::token, false);

    private HttpMethod httpMethod;
    private String uri;
    private Handler<RoutingContext> handler;
    private boolean auth;
}
