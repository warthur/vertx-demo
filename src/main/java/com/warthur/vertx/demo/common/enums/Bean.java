package com.warthur.vertx.demo.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author warthur
 * @date 2019/10/12
 */
@Getter
@AllArgsConstructor
public enum Bean {

    REDIS_CLIENT("redis_client"),
    MYSQL_CLIENT("mysql_client");

    private String name;
}
