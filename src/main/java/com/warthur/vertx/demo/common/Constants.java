package com.warthur.vertx.demo.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author warthur
 * @date 2019/04/09
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    public static final String REDIS_TODO_KEY = "VERT_TODO";

    public final static String LF = "\n";
    public final static String BS = " ";



}
