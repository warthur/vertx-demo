package com.warthur.vertx.demo.config;

import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.http.HttpVersion;
import io.vertx.core.json.JsonObject;

import java.util.Arrays;
import java.util.List;

/**
 * @author warthur
 * @date 2019/10/11
 */
public final class Config {

    private static final String HTTP = "http";
    private static final String HOST = "host";
    private static final String PORT = "port";
    private static final int PORT_DEFAULT = 8080;

    private final HttpServerOptions httpServerOptions;

    public Config(JsonObject config) {
        JsonObject http = config.getJsonObject(HTTP);
        String host = http.getString(HOST);
        int port = http.getInteger(PORT, PORT_DEFAULT);

        List<HttpVersion> versions = Arrays.asList(HttpVersion.HTTP_1_1, HttpVersion.HTTP_2);

        httpServerOptions = new HttpServerOptions();
        httpServerOptions.setAlpnVersions(versions);
        httpServerOptions.setHost(host);
        httpServerOptions.setPort(port);
        httpServerOptions.setLogActivity(true);
    }

    public HttpServerOptions getHttpServerOptions() {
        return httpServerOptions;
    }
}
